package com.desafio.api.apidesafio.utilitarios;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class SenhaUtilitarioTest {
    
    private static final String SENHA = "senha";
    private final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    @Test
    public void testGerarCriptograma() throws Exception {
        String criptograma = SenhaUtilitario.gerarCriptograma(SENHA);
        assertTrue(bCryptPasswordEncoder.matches(SENHA, criptograma));
    }
}
