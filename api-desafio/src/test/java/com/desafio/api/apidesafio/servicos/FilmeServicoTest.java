package com.desafio.api.apidesafio.servicos;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.desafio.api.apidesafio.entidades.Filme;
import com.desafio.api.apidesafio.repositorios.FilmeRepositorio;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
public class FilmeServicoTest {

    @MockBean
    private FilmeRepositorio filmeRepositorio;

    @Autowired
    private FilmeServico filmeServico;

    @BeforeEach
    public void setUp() throws Exception {
        BDDMockito.given(this.filmeRepositorio.save(Mockito.any(Filme.class))).willReturn(new Filme());
    }

    @Test
    public void testInserirFilme(){
        Filme filme = this.filmeServico.inserirFilme(new Filme());
        assertNotNull(filme);
    }
}
