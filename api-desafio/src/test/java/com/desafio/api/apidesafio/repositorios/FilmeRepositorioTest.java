package com.desafio.api.apidesafio.repositorios;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Date;

import com.desafio.api.apidesafio.entidades.Filme;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
public class FilmeRepositorioTest {
    
    @Autowired
    private FilmeRepositorio filmeRepositorio;

    private static final String nomeFilme = "Os Vingadores";
    private static final String nomeDiretor = "Joss Whedon";
    private static final String nomeAtores = "Robert Downey Jr;Chris Evans;Scarlett Johansson;";
    private static final String nomeGeneros = "Ação;Ficção Científica;";
    private static final PageRequest pagina = PageRequest.of(0, 10);

    @BeforeEach
    public void setUp() throws Exception {
        Filme filme = new Filme();
        filme.setNome(nomeFilme);
        filme.setDataLancamento(new Date());
        filme.setDiretor(nomeDiretor);
        filme.setAtores(nomeAtores);
        filme.setGeneros(nomeGeneros);
        this.filmeRepositorio.save(filme);
    }

    @AfterEach
    public final void tearDown(){
        this.filmeRepositorio.deleteAll();
    }

    @Test
    public void testObterPorNome(){
        Page<Filme> filmes = this.filmeRepositorio.findByNome("Vingadores", pagina);
        assertEquals(1, filmes.getTotalElements());
    }

    @Test
    public void testObterPorDiretor(){
        Page<Filme> filmes = this.filmeRepositorio.findByDiretor("Joss", pagina);
        assertEquals(1, filmes.getTotalElements());
    }

    @Test
    public void testObterPorGeneros(){
        Page<Filme> filmes = this.filmeRepositorio.findByGenero("Ação", pagina);
        assertEquals(1, filmes.getTotalElements());
    }

    @Test
    public void testObterPorAtores(){
        Page<Filme> filmes = this.filmeRepositorio.findByAtor("Scarlett Johansson", pagina);
        assertEquals(1, filmes.getTotalElements());
    }
}
