package com.desafio.api.apidesafio.repositorios;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Date;
import java.util.List;

import com.desafio.api.apidesafio.entidades.Usuario;
import com.desafio.api.apidesafio.enumerados.Cargo;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
public class UsuarioRepositorioTest {
    
    @Autowired
    private UsuarioRepositorio usuarioRepositorio;

    @BeforeEach
    public void setUp() throws Exception {
        this.usuarioRepositorio.deleteAll();
        Usuario usuario1 = new Usuario();
        usuario1.setNome("Leonardo");
        usuario1.setDataNascimento(new Date());
        usuario1.setCargo(Cargo.ROLE_ADMIN);
        usuario1.setEmail("leonardo@mail.com");
        usuario1.setSenha("senha");
        this.usuarioRepositorio.save(usuario1);
        Usuario usuario2 = new Usuario();
        usuario2.setNome("Nascimento");
        usuario2.setDataNascimento(new Date());
        usuario2.setDataDesativacao(new Date());
        usuario2.setCargo(Cargo.ROLE_USUARIO);
        usuario2.setEmail("nascimento@mail.com");
        usuario2.setSenha("senha");
        this.usuarioRepositorio.save(usuario2);
        Usuario usuario3 = new Usuario();
        usuario3.setNome("Souza");
        usuario3.setDataNascimento(new Date());
        usuario3.setCargo(Cargo.ROLE_USUARIO);
        usuario3.setEmail("souza@mail.com");
        usuario3.setSenha("senha");
        this.usuarioRepositorio.save(usuario3);
        Usuario usuario4 = new Usuario();
        usuario4.setNome("Azuos");
        usuario4.setDataNascimento(new Date());
        usuario4.setCargo(Cargo.ROLE_USUARIO);
        usuario4.setEmail("azuos@mail.com");
        usuario4.setSenha("senha");
        this.usuarioRepositorio.save(usuario4);
    }

    @AfterEach
    public final void tearDown(){
        this.usuarioRepositorio.deleteAll();
    }

    @Test
    public void testObterUsuariosAtivos(){
        List<Usuario> usuarios = this.usuarioRepositorio.findAllUsuariosAtivos();
        assertEquals(2, usuarios.size()); 
    }

    @Test
    public void testObterUsuariosAtivosPaginado(){
        PageRequest pagina = PageRequest.of(0, 10);
        Page<Usuario> usuariosPaginados = this.usuarioRepositorio.findAllUsuariosAtivos(pagina);
        assertEquals(2, usuariosPaginados.getTotalElements()); 
    }
}
