package com.desafio.api.apidesafio;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
class ApiDesafioApplicationTests {

	@Test
	void contextLoads() {
	}

}
