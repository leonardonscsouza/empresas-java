package com.desafio.api.apidesafio.servicos;

import com.desafio.api.apidesafio.entidades.Filme;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface FilmeServico {

    /**
     * Insere um novo filme na base de dados.
     * 
     * @param filme
     * @return Filme
     */
    Filme inserirFilme(Filme filme);

    /**
     * Retorna uma lista de filmes dado nome
     * 
     * @param nome
     * @return Page<Filme>
     */
    Page<Filme> consultarFilmesPorNome(String nome, PageRequest pageRequest);

    /**
     * Retorna uma lista de filmes dado diretor
     * 
     * @param diretor
     * @return Page<Filme>
     */
    Page<Filme> consultarFilmesPorDiretor(String diretor, PageRequest pageRequest);

     /**
     * Retorna uma lista de filmes dado ator
     * 
     * @param ator
     * @return Page<Filme>
     */
    Page<Filme> consultarFilmesPorAtor(String ator, PageRequest pageRequest);

    /**
     * Retorna uma lista de filmes dado genero
     * 
     * @param genero
     * @return Page<Filme>
     */
    Page<Filme> consultarFilmesPorGenero(String genero, PageRequest pageRequest);

    /**
     * Retorna uma lista de filmes dado filtro
     * 
     * @param nome
     * @param diretor
     * @param ator
     * @param genero
     * @return Page<Filme>
     */
    Page<Filme> consultarFilmes(String nome, String diretor, String ator, String genero, PageRequest pageRequest);


}
