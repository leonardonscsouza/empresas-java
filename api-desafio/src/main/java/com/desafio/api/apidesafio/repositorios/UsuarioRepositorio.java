package com.desafio.api.apidesafio.repositorios;

import java.util.List;

import com.desafio.api.apidesafio.entidades.Usuario;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;


public interface UsuarioRepositorio extends JpaRepository<Usuario, Long> {

    Usuario findByEmail(String email);

    @Transactional(readOnly = true)
    @Query(value = "SELECT * FROM Usuario WHERE cargo = 'ROLE_USUARIO' AND data_desativacao IS NULL ORDER BY nome ASC", nativeQuery = true)
    List<Usuario> findAllUsuariosAtivos();

    @Transactional(readOnly = true)
    @Query(value = "SELECT * FROM Usuario WHERE cargo = 'ROLE_USUARIO' AND data_desativacao IS NULL ORDER BY nome ASC", nativeQuery = true)
    Page<Usuario> findAllUsuariosAtivos(Pageable pageable);

    @Transactional
    @Modifying
    @Query(value = "UPDATE Usuario SET data_desativacao = NULL WHERE id = :id")
    void ativarUsuario(@Param("id")Long id);

    @Transactional
    @Modifying
    @Query(value = "UPDATE Usuario SET data_desativacao = NOW() WHERE id = :id")
    void desativarUsuario(@Param("id")Long id);

}
