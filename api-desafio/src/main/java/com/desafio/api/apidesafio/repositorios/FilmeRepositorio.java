package com.desafio.api.apidesafio.repositorios;

import com.desafio.api.apidesafio.entidades.Filme;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
public interface FilmeRepositorio extends JpaRepository<Filme, Long> {
    
    @Query(value = "SELECT f.*, j.classificacao FROM Filme f LEFT JOIN (SELECT v.filme_id, avg(valor) as classificacao FROM Voto v GROUP BY v.filme_id) j ON j.filme_id = f.id WHERE nome LIKE %:nome% ORDER BY classificacao DESC, nome ASC", nativeQuery = true)
    Page<Filme> findByNome(@Param("nome") String nome, Pageable pageable);

    @Query(value = "SELECT f.*, j.classificacao FROM Filme f LEFT JOIN (SELECT v.filme_id, avg(valor) as classificacao FROM Voto v GROUP BY v.filme_id) j ON j.filme_id = f.id WHERE diretor LIKE %:diretor% ORDER BY classificacao DESC, nome ASC", nativeQuery = true)
    Page<Filme> findByDiretor(@Param("diretor") String diretor, Pageable pageable);

    @Query(value = "SELECT f.*, j.classificacao FROM Filme f LEFT JOIN (SELECT v.filme_id, avg(valor) as classificacao FROM Voto v GROUP BY v.filme_id) j ON j.filme_id = f.id WHERE atores LIKE %:ator% ORDER BY classificacao DESC, nome ASC", nativeQuery = true)
    Page<Filme> findByAtor(@Param("ator") String ator, Pageable pageable);

    @Query(value = "SELECT f.*, j.classificacao FROM Filme f LEFT JOIN (SELECT v.filme_id, avg(valor) as classificacao FROM Voto v GROUP BY v.filme_id) j ON j.filme_id = f.id WHERE generos LIKE %:genero% ORDER BY classificacao DESC, nome ASC", nativeQuery = true)
    Page<Filme> findByGenero(@Param("genero") String genero, Pageable pageable);

    @Query(value = "SELECT f.*, j.classificacao FROM Filme f LEFT JOIN (SELECT v.filme_id, avg(valor) as classificacao FROM Voto v GROUP BY v.filme_id) j ON j.filme_id = f.id WHERE nome LIKE :nome OR diretor LIKE :diretor OR atores LIKE :ator OR generos LIKE :genero ORDER BY classificacao DESC, nome ASC", nativeQuery = true)
    Page<Filme> findByFiltro(@Param("nome") String nome, @Param("diretor") String diretor,
        @Param("ator") String ator, @Param("genero") String genero, Pageable pageable);

}
