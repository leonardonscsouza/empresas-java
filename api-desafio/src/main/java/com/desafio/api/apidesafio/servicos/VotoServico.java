package com.desafio.api.apidesafio.servicos;

import java.util.Optional;

import com.desafio.api.apidesafio.entidades.Voto;

public interface VotoServico {

    /**
     * Realiza um voto na base de dados.
     * 
     * @param voto
     */
    void realizarVoto(Voto voto);

    /**
     * Retorna um voto
     * 
     * @return Optional<Voto>
     */
    Optional<Voto> consultarVoto(Voto voto);
    
}
