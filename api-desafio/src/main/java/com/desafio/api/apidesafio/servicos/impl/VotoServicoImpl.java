package com.desafio.api.apidesafio.servicos.impl;

import java.util.Optional;

import com.desafio.api.apidesafio.entidades.Voto;
import com.desafio.api.apidesafio.repositorios.VotoRepositorio;
import com.desafio.api.apidesafio.servicos.VotoServico;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VotoServicoImpl implements VotoServico{

    private static final Logger log = LoggerFactory.getLogger(VotoServicoImpl.class);

    @Autowired
    private VotoRepositorio votoRepositorio;

    @Override
    public void realizarVoto(Voto voto) {
        log.info("Realizando voto. {}", voto);
        this.votoRepositorio.save(voto);
    }

    @Override
    public Optional<Voto> consultarVoto(Voto voto) {
        log.info("Obtendo voto");
        return this.votoRepositorio.findVotoByIdUsuarioFilme(voto.getUsuario().getId(), voto.getFilme().getId());
    }
    
}
