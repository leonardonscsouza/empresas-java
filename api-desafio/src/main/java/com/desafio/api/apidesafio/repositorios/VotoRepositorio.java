package com.desafio.api.apidesafio.repositorios;

import java.util.Optional;

import com.desafio.api.apidesafio.entidades.Voto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface VotoRepositorio extends JpaRepository<Voto, Long> {
    
    @Transactional(readOnly = true)
    @Query(value = "SELECT * FROM Voto WHERE usuario_id = :usuarioId AND filme_id = :filmeId", nativeQuery = true)
    Optional<Voto> findVotoByIdUsuarioFilme(@Param("usuarioId")Long usuarioId, @Param("filmeId")Long filmeId);
}
