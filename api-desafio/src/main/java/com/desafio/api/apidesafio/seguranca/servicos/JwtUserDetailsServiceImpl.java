package com.desafio.api.apidesafio.seguranca.servicos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.desafio.api.apidesafio.entidades.Usuario;
import com.desafio.api.apidesafio.seguranca.JwtUserFactory;
import com.desafio.api.apidesafio.servicos.UsuarioServico;

@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UsuarioServico usuarioServico;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario usuario = usuarioServico.consultarUsuarioPorEmail(username);

		if (usuario != null) {
			return JwtUserFactory.create(usuario);
		}

		throw new UsernameNotFoundException("Usuário não encontrado.");
	}

}
