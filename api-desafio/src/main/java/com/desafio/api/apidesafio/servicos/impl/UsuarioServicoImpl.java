package com.desafio.api.apidesafio.servicos.impl;

import java.util.List;
import java.util.Optional;

import com.desafio.api.apidesafio.entidades.Usuario;
import com.desafio.api.apidesafio.repositorios.UsuarioRepositorio;
import com.desafio.api.apidesafio.servicos.UsuarioServico;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class UsuarioServicoImpl implements UsuarioServico {

    private static final Logger log = LoggerFactory.getLogger(UsuarioServicoImpl.class);

    @Autowired
    private UsuarioRepositorio usuarioRepositorio;

    @Override
    public Usuario inserirUsuario(Usuario usuario) {
        log.info("Inserindo usuário. {}", usuario);
        return this.usuarioRepositorio.save(usuario);
    }

    @Override
    public Usuario consultarUsuarioPorEmail(String email) {
        log.info("Obtendo usuário a partir do email. {}", email);
        return this.usuarioRepositorio.findByEmail(email);
    }

    @Override
    public List<Usuario> consultarUsuariosAtivos() {
        log.info("Obtendo todos os usuários ativos.");
        return this.usuarioRepositorio.findAllUsuariosAtivos();
    }

    @Override
    public Page<Usuario> consultarUsuariosAtivos(PageRequest pageRequest) {
        log.info("Obtendo todos os usuários ativos.");
        return this.usuarioRepositorio.findAllUsuariosAtivos(pageRequest);
    }

    @Override
    public void desativarUsuario(Long id) {
        log.info("Desativando usuário com id {}.", id);
        this.usuarioRepositorio.desativarUsuario(id);
    }

    @Override
    public void ativarUsuario(Long id) {
        log.info("Ativando usuário com id {}.", id);
        this.usuarioRepositorio.ativarUsuario(id);
    }

    @Override
    public Usuario alterarUsuario(Usuario usuario) {
        log.info("Alterados dados do usuário {}.", usuario);
        return this.usuarioRepositorio.save(usuario);
    }

    @Override
    public Optional<Usuario> obterUsuarioPorId(Long id) {
        log.info("Obtendo usuário a partir do id. {}", id);
        return this.usuarioRepositorio.findById(id);
    }


    
}
