package com.desafio.api.apidesafio.controlador;

import com.desafio.api.apidesafio.entidades.Filme;
import com.desafio.api.apidesafio.response.Response;
import com.desafio.api.apidesafio.servicos.FilmeServico;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;

@RestController
@RequestMapping("/api/filmes")
@CrossOrigin(origins = "*")
@Api(tags="Filmes API")
public class FilmeControlador {

    private static final Logger log = LoggerFactory.getLogger(FilmeControlador.class);

    @Autowired
    private FilmeServico filmeServico;

     /**
	 * Cadastra um novo filme no sistema.
	 * 
	 * @param filme
	 * @param result
	 * @return ResponseEntity<Response<Filme>>
	 * @throws NoSuchAlgorithmException
	 */
	@PostMapping
    @PreAuthorize("hasAnyRole('ADMIN')")
	public ResponseEntity<Response<Filme>> inserirUsuario(@RequestBody Filme filme,
			BindingResult result) {
		log.info("Inserindo filme: {}", filme.toString());
		Response<Filme> response = new Response<Filme>();
        if(filme.getNome() == null){
            result.addError(new ObjectError("filme", "O campo nome é requerido."));
        }
        if(filme.getDiretor() == null){
            result.addError(new ObjectError("filme", "O campo diretor é requerido."));
        }
        if(filme.getAtores() == null){
            result.addError(new ObjectError("filme", "O campo atores é requerido."));
        }
        if(filme.getGeneros() == null){
            result.addError(new ObjectError("filme", "O campo generos é requerido."));
        }
        if(filme.getDataLancamento() == null){
            result.addError(new ObjectError("filme", "O campo data de lançamento é requerido."));
        }
		if (result.hasErrors()) {
			log.error("Erro validando dados do filme: {}", result.getAllErrors());
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.badRequest().body(response);
		}
		response.setData(this.filmeServico.inserirFilme(filme));
		return ResponseEntity.ok(response);
	}

    /**
	 * Obtém filmes a partir de um filtro no sistema.
	 * 
	 * @return ResponseEntity<Response<Page<Filme>>>
	 */
	@GetMapping
	public ResponseEntity<Response<Page<Filme>>> obterFilmesPorFiltro(
        @RequestParam(value = "nome", required = false) String nome,
        @RequestParam(value = "diretor", required = false) String diretor,
        @RequestParam(value = "ator", required = false) String ator,
        @RequestParam(value = "genero", required = false) String genero,
		@RequestParam(value = "pagina", defaultValue = "0") int pagina,
		@RequestParam(value = "qtdRegistros", defaultValue = "10") int qtdRegistros) {
		log.info("Obtendo filmes na página {}.", pagina);
		Response<Page<Filme>> response = new Response<Page<Filme>>();
		PageRequest pageRequest = PageRequest.of(pagina, qtdRegistros);
        if(nome == null && diretor == null && ator == null && genero == null){
            nome = "%%";
            diretor = "%%";
            ator = "%%";
            genero = "%%";
        } else {
            if(nome == null){
                nome = "NULL";
            } else {
                nome = "%"+nome+"%";
            }
            if(diretor == null){
                diretor = "NULL";
            } else {
                diretor = "%"+diretor+"%";
            }
            if(ator == null){
                ator = "NULL";
            } else {
                ator = "%"+ator+"%";
            }
            if(genero == null){
                genero = "NULL";
            } else {
                genero = "%"+genero+"%";
            }
        }
		response.setData(this.filmeServico.consultarFilmes(nome, diretor, ator, genero, pageRequest));
		return ResponseEntity.ok(response);
	}

    /**
	 * Obtém filmes a partir de um nome no sistema.
	 * 
	 * @return ResponseEntity<Response<Page<Filme>>>
	 */
	@GetMapping(value = "/nome/{nome}")
	public ResponseEntity<Response<Page<Filme>>> obterFilmesPorNome(
        @PathVariable(value = "nome") String nome,
		@RequestParam(value = "pagina", defaultValue = "0") int pagina,
		@RequestParam(value = "qtdRegistros", defaultValue = "10") int qtdRegistros) {
		log.info("Obtendo filmes na página {}.", pagina);
		Response<Page<Filme>> response = new Response<Page<Filme>>();
		PageRequest pageRequest = PageRequest.of(pagina, qtdRegistros);
		response.setData(this.filmeServico.consultarFilmesPorNome(nome, pageRequest));
		return ResponseEntity.ok(response);
	}

    /**
	 * Obtém filmes a partir de um diretor no sistema.
	 * 
	 * @return ResponseEntity<Response<Page<Filme>>>
	 */
	@GetMapping(value = "/diretor/{diretor}")
	public ResponseEntity<Response<Page<Filme>>> obterFilmesPorDiretor(
        @PathVariable(value = "diretor") String diretor,
		@RequestParam(value = "pagina", defaultValue = "0") int pagina,
		@RequestParam(value = "qtdRegistros", defaultValue = "10") int qtdRegistros) {
		log.info("Obtendo filmes na página {}.", pagina);
		Response<Page<Filme>> response = new Response<Page<Filme>>();
		PageRequest pageRequest = PageRequest.of(pagina, qtdRegistros);
		response.setData(this.filmeServico.consultarFilmesPorDiretor(diretor, pageRequest));
		return ResponseEntity.ok(response);
	}

    /**
	 * Obtém filmes a partir de um ator no sistema.
	 * 
	 * @return ResponseEntity<Response<Page<Filme>>>
	 */
	@GetMapping(value = "/ator/{ator}")
	public ResponseEntity<Response<Page<Filme>>> obterFilmesPorAtor(
        @PathVariable(value = "ator") String ator,
		@RequestParam(value = "pagina", defaultValue = "0") int pagina,
		@RequestParam(value = "qtdRegistros", defaultValue = "10") int qtdRegistros) {
		log.info("Obtendo filmes na página {}.", pagina);
		Response<Page<Filme>> response = new Response<Page<Filme>>();
		PageRequest pageRequest = PageRequest.of(pagina, qtdRegistros);
		response.setData(this.filmeServico.consultarFilmesPorAtor(ator, pageRequest));
		return ResponseEntity.ok(response);
	}

    /**
	 * Obtém filmes a partir de um genero no sistema.
	 * 
	 * @return ResponseEntity<Response<Page<Filme>>>
	 */
	@GetMapping(value = "/genero/{genero}")
	public ResponseEntity<Response<Page<Filme>>> obterFilmesPorGenero(
        @PathVariable(value = "genero") String genero,
		@RequestParam(value = "pagina", defaultValue = "0") int pagina,
		@RequestParam(value = "qtdRegistros", defaultValue = "10") int qtdRegistros) {
		log.info("Obtendo filmes na página {}.", pagina);
		Response<Page<Filme>> response = new Response<Page<Filme>>();
		PageRequest pageRequest = PageRequest.of(pagina, qtdRegistros);
		response.setData(this.filmeServico.consultarFilmesPorGenero(genero, pageRequest));
		return ResponseEntity.ok(response);
	}
}
