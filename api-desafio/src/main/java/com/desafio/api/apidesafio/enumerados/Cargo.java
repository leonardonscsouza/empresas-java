package com.desafio.api.apidesafio.enumerados;

public enum Cargo {
    ROLE_ADMIN,
    ROLE_USUARIO;
}
