package com.desafio.api.apidesafio.utilitarios;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class SenhaUtilitario {

    private static final Logger log = LoggerFactory.getLogger(SenhaUtilitario.class);
    
    public static String gerarCriptograma(String senha) {
        if(senha == null) {
            return senha;
        }
        log.info("Gerando criptograma com o Bcrypt.");
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder.encode(senha);
    }
}
