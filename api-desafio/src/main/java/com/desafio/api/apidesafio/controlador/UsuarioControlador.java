package com.desafio.api.apidesafio.controlador;

import java.security.NoSuchAlgorithmException;
import java.util.Optional;

import javax.validation.Valid;

import com.desafio.api.apidesafio.entidades.Usuario;
import com.desafio.api.apidesafio.enumerados.Cargo;
import com.desafio.api.apidesafio.response.Response;
import com.desafio.api.apidesafio.servicos.UsuarioServico;
import com.desafio.api.apidesafio.to.UsuarioTO;
import com.desafio.api.apidesafio.utilitarios.SenhaUtilitario;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;

@RestController
@RequestMapping("/api/usuarios")
@CrossOrigin(origins = "*")
@Api(tags="Usuarios API")
public class UsuarioControlador {

    private static final Logger log = LoggerFactory.getLogger(UsuarioControlador.class);

    @Autowired
    private UsuarioServico usuarioServico;

    public UsuarioControlador() {
    }

    /**
	 * Cadastra um novo usuário no sistema.
	 * 
	 * @param usuarioTO
	 * @param result
	 * @return ResponseEntity<Response<UsuarioTO>>
	 * @throws NoSuchAlgorithmException
	 */
	@PostMapping
	public ResponseEntity<Response<UsuarioTO>> inserirUsuario(@Valid @RequestBody UsuarioTO usuarioTO,
			BindingResult result) throws NoSuchAlgorithmException {
		log.info("Inserindo usuário: {}", usuarioTO.toString());
		Response<UsuarioTO> response = new Response<UsuarioTO>();
		validarDadosExistentes(usuarioTO, result);
		Usuario usuario = this.converterUsuarioTOParaEntidade(usuarioTO, result, Cargo.ROLE_USUARIO);
		if (result.hasErrors()) {
			log.error("Erro validando dados do usuário: {}", result.getAllErrors());
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.badRequest().body(response);
		}
		response.setData(this.converterEntidadeParaUsuarioTO(this.usuarioServico.inserirUsuario(usuario)));
		return ResponseEntity.ok(response);
	}

	/**
	 * Cadastra um novo usuário admin no sistema.
	 * 
	 * @param usuarioTO
	 * @param result
	 * @return ResponseEntity<Response<UsuarioTO>>
	 * @throws NoSuchAlgorithmException
	 */
	@PostMapping(value = "/admin")
	@PreAuthorize("hasAnyRole('ADMIN')")
	public ResponseEntity<Response<UsuarioTO>> inserirUsuarioAdmin(@Valid @RequestBody UsuarioTO usuarioTO,
			BindingResult result) throws NoSuchAlgorithmException {
		log.info("Inserindo usuário: {}", usuarioTO.toString());
		Response<UsuarioTO> response = new Response<UsuarioTO>();
		validarDadosExistentes(usuarioTO, result);
		Usuario usuario = this.converterUsuarioTOParaEntidade(usuarioTO, result, Cargo.ROLE_ADMIN);
		if (result.hasErrors()) {
			log.error("Erro validando dados do usuário: {}", result.getAllErrors());
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.badRequest().body(response);
		}
		response.setData(this.converterEntidadeParaUsuarioTO(this.usuarioServico.inserirUsuario(usuario)));
		return ResponseEntity.ok(response);
	}

	/**
	 * Obtém usuários não administradores ativos no sistema.
	 * 
	 * @return ResponseEntity<Response<Page<Usuario>>>
	 * @throws NoSuchAlgorithmException
	 */
	@GetMapping(value = "/ativos")
	@PreAuthorize("hasAnyRole('ADMIN')")
	public ResponseEntity<Response<Page<Usuario>>> obterUsuariosAtivos(
		@RequestParam(value = "pagina", defaultValue = "0") int pagina,
		@RequestParam(value = "qtdRegistros", defaultValue = "100") int qtdRegistros) {
		log.info("Obtendo usuários na página {}.", pagina);
		Response<Page<Usuario>> response = new Response<Page<Usuario>>();
		PageRequest pageRequest = PageRequest.of(pagina, qtdRegistros);
		response.setData(this.usuarioServico.consultarUsuariosAtivos(pageRequest));
		return ResponseEntity.ok(response);
	}

	/**
	 * Altera um usuário no sistema.
	 * 
	 * @param id
	 * @param usuarioTO
	 * @param result
	 * @return ResponseEntity<Response<UsuarioTO>>
	 * @throws NoSuchAlgorithmException
	 */
	@PutMapping(value = "/{id}")
	public ResponseEntity<Response<UsuarioTO>> alterarUsuario(@PathVariable("id") Long id,
		@Valid @RequestBody UsuarioTO usuarioTO, BindingResult result) throws NoSuchAlgorithmException {
		log.info("Alterando usuário com id: {}", id);
		Response<UsuarioTO> response = new Response<UsuarioTO>();
		usuarioTO.setId(id);
		Usuario usuario = isUsuarioValido(usuarioTO, result);
		//Validações...
		if(usuarioTO.getNome() != null){
			usuario.setNome(usuarioTO.getNome());
		}
		if(usuarioTO.getEmail() != null){
			usuario.setEmail(usuarioTO.getEmail());
		}
		if(usuarioTO.getSenha() != null){
			usuario.setSenha(SenhaUtilitario.gerarCriptograma(usuarioTO.getSenha()));
		}
		if(usuarioTO.getDataNascimento() != null){
			usuario.setDataNascimento(usuarioTO.getDataNascimento());
		}
		if (result.hasErrors()) {
			log.error("Erro validando dados do usuário: {}", result.getAllErrors());
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.badRequest().body(response);
		}
		response.setData(this.converterEntidadeParaUsuarioTO(this.usuarioServico.alterarUsuario(usuario)));
		return ResponseEntity.ok(response);
	}

	/**
	 * Desativa um usuários no sistema.
	 * 
	 * @param id
	 * 
	 * @throws NoSuchAlgorithmException
	 */
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Response<String>> desativarUsuario(@PathVariable("id") Long id, 
	@RequestBody (required=false) UsuarioTO usuarioBody, BindingResult result) {
		log.info("Desativando usuário com id: {}", id);
		Response<String> response = new Response<String>();
		UsuarioTO usuarioTO = new UsuarioTO();
		usuarioTO.setId(id);
		isUsuarioValido(usuarioTO, result);
		if (result.hasErrors()) {
			log.error("Erro validando dados do usuário: {}", result.getAllErrors());
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.badRequest().body(response);
		}
		this.usuarioServico.desativarUsuario(id);
		response.setData("Usuário desativado com sucesso!");
		return ResponseEntity.ok(response);
	}

	/**
	 * Desativa um usuários no sistema.
	 * 
	 * @param id
	 * 
	 * @throws NoSuchAlgorithmException
	 */
	@PutMapping(value = "/ativar/{id}")
	@PreAuthorize("hasAnyRole('ADMIN')")
	public ResponseEntity<Response<String>> ativarUsuario(@PathVariable("id") Long id, 
	@RequestBody (required=false) UsuarioTO usuarioBody, BindingResult result) {
		log.info("Ativando usuário com id: {}", id);
		Response<String> response = new Response<String>();
		UsuarioTO usuarioTO = new UsuarioTO();
		usuarioTO.setId(id);
		isUsuarioValido(usuarioTO, result);
		if (result.hasErrors()) {
			log.error("Erro validando dados do usuário: {}", result.getAllErrors());
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.badRequest().body(response);
		}
		this.usuarioServico.ativarUsuario(id);
		response.setData("Usuário ativado com sucesso!");
		return ResponseEntity.ok(response);
	}

    /**
	 * Verifica se o usuário já existe na base de dados.
	 * 
	 * @param usuarioTO
	 * @param result
	 */
	private void validarDadosExistentes(UsuarioTO usuarioTO, BindingResult result) {
		Usuario usuario = this.usuarioServico.consultarUsuarioPorEmail(usuarioTO.getEmail());
        if(usuario != null){
            result.addError(new ObjectError("usuario", "Usuário com este mesmo email já existente."));
        }
	}

    /**
	 * Converte os dados do UsuarioTO para Usuario.
	 * 
	 * @param usuarioTO
	 * @return Usuario
	 */
	private Usuario converterUsuarioTOParaEntidade(UsuarioTO usuarioTO, BindingResult result, Cargo cargo) {
		Usuario usuario = new Usuario();
		usuario.setId(usuarioTO.getId());
        usuario.setNome(usuarioTO.getNome());
        usuario.setEmail(usuarioTO.getEmail());
        usuario.setSenha(SenhaUtilitario.gerarCriptograma(usuarioTO.getSenha()));
		usuario.setCargo(cargo);
        usuario.setDataNascimento(usuarioTO.getDataNascimento());
		return usuario;
	}

    /**
	 * Converte os dados do UsuarioTO para Usuario.
	 * 
	 * @param usuarioTO
	 * @return Usuario
	 */
	private UsuarioTO converterEntidadeParaUsuarioTO(Usuario usuario) {
		UsuarioTO usuarioTO = new UsuarioTO();
		usuarioTO.setId(usuario.getId());
        usuarioTO.setNome(usuario.getNome());
        usuarioTO.setEmail(usuario.getEmail());
        usuarioTO.setDataNascimento(usuario.getDataNascimento());
		return usuarioTO;
	}

	/**
	 * Verifica se o usuário já existe na base de dados.
	 * 
	 * @param usuarioTO
	 * @param result
	 */
	private Usuario isUsuarioValido(UsuarioTO usuarioTO, BindingResult result) {
		Optional<Usuario> usuario = this.usuarioServico.obterUsuarioPorId(usuarioTO.getId());
        if(!usuario.isPresent()){
            result.addError(new ObjectError("usuario", "Usuário com id informado não existe."));
			return null;
        }
		return usuario.get();
	}

}
