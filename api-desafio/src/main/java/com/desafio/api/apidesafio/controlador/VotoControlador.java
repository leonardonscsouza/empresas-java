package com.desafio.api.apidesafio.controlador;

import java.util.Optional;

import com.desafio.api.apidesafio.entidades.Voto;
import com.desafio.api.apidesafio.response.Response;
import com.desafio.api.apidesafio.servicos.VotoServico;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;

@RestController
@RequestMapping("/api/votos")
@CrossOrigin(origins = "*")
@Api(tags="Votos API")
public class VotoControlador {
    
    private static final Logger log = LoggerFactory.getLogger(VotoControlador.class);

    @Autowired
    private VotoServico votoServico;

     /**
	 * Realiza um novo voto no sistema.
	 * 
	 * @param voto
	 * @param result
	 * @return ResponseEntity<Response<Voto>>
	 */
	@PostMapping
    @PreAuthorize("hasAnyRole('USUARIO')")
	public ResponseEntity<Response<String>> realizarVoto(@RequestBody Voto voto,
			BindingResult result) {
		log.info("Realizando voto: {}", voto.toString());
		Response<String> response = new Response<String>();
        if(voto.getFilme() == null || voto.getFilme().getId() == null){
            result.addError(new ObjectError("voto", "O campo id do filme é requerido."));
        }
        if(voto.getUsuario() == null || voto.getUsuario().getId() == null){
            result.addError(new ObjectError("filme", "O campo id do usuário é requerido."));
        }
        if(voto.getValor() < 0 || voto.getValor() > 4){
            result.addError(new ObjectError("filme", "O campo valor é requerido. E deve ser estar entre os valores 0-4"));
        }
		if (result.hasErrors()) {
			log.error("Erro validando dados do voto: {}", result.getAllErrors());
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.badRequest().body(response);
		}
        //Consulta para obter objeto completo para possível update
        Voto consultarVoto = consultaVoto(voto);
        if(consultarVoto != null){
            consultarVoto.setValor(voto.getValor());
            voto = consultarVoto;
        }
        this.votoServico.realizarVoto(voto);
		response.setData("Voto realizado com sucesso!");
		return ResponseEntity.ok(response);
	}

	/**
	 * Verifica se o usuário já votou em um filme.
	 * 
	 * @param usuarioTO
	 * @param result
	 */
	private Voto consultaVoto(Voto voto) {
		Optional<Voto> consultaVoto = this.votoServico.consultarVoto(voto);
        if(consultaVoto.isPresent()){
			return consultaVoto.get();
        }
		return null;
	}
}
