package com.desafio.api.apidesafio.servicos.impl;

import com.desafio.api.apidesafio.entidades.Filme;
import com.desafio.api.apidesafio.repositorios.FilmeRepositorio;
import com.desafio.api.apidesafio.servicos.FilmeServico;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class FilmeServicoImpl implements FilmeServico {

    private static final Logger log = LoggerFactory.getLogger(FilmeServicoImpl.class);

    @Autowired
    private FilmeRepositorio filmeRepositorio;

    @Override
    public Filme inserirFilme(Filme filme) {
        log.info("Inserindo filme. {}", filme);
        return this.filmeRepositorio.save(filme);
    }

    @Override
    public Page<Filme> consultarFilmesPorNome(String nome, PageRequest pageRequest) {
        log.info("Obtendo filmes por nome: {}", nome);
        return this.filmeRepositorio.findByNome(nome, pageRequest);
    }

    @Override
    public Page<Filme> consultarFilmesPorDiretor(String diretor, PageRequest pageRequest) {
        log.info("Obtendo filmes por diretor: {}", diretor);
        return this.filmeRepositorio.findByDiretor(diretor, pageRequest);
    }

    @Override
    public Page<Filme> consultarFilmesPorAtor(String ator, PageRequest pageRequest) {
        log.info("Obtendo filmes por ator: {}", ator);
        return this.filmeRepositorio.findByAtor(ator, pageRequest);
    }

    @Override
    public Page<Filme> consultarFilmesPorGenero(String genero, PageRequest pageRequest) {
        log.info("Obtendo filmes por genero: {}", genero);
        return this.filmeRepositorio.findByGenero(genero, pageRequest);
    }

    @Override
    public Page<Filme> consultarFilmes(String nome, String diretor, String ator, String genero, PageRequest pageRequest) {
        log.info("Obtendo filmes por nome: {}. Diretor: {}. Ator: {}. Genero: {}", nome, diretor, ator, genero);
        return this.filmeRepositorio.findByFiltro(nome, diretor, ator, genero, pageRequest);
    }
    
}
