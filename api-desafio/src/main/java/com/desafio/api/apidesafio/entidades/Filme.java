package com.desafio.api.apidesafio.entidades;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "filme")
public class Filme {

    private Long id;
    private String nome;
    private String diretor;
    private String generos;
    private String atores;
    private Date dataLancamento;
    private Double classificacao;

    public Filme() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "nome", nullable = false)
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Column(name = "generos", nullable = false)
    public String getGeneros() {
        return generos;
    }

    public void setGeneros(String generos) {
        this.generos = generos;
    }

    @Column(name = "atores", nullable = false)
    public String getAtores() {
        return atores;
    }

    public void setAtores(String atores) {
        this.atores = atores;
    }

    @Column(name = "diretor", nullable = false)
    public String getDiretor() {
        return diretor;
    }

    public void setDiretor(String diretor) {
        this.diretor = diretor;
    }

    @Column(name = "data_lancamento", nullable = false)
    public Date getDataLancamento() {
        return dataLancamento;
    }

    public void setDataLancamento(Date dataLancamento) {
        this.dataLancamento = dataLancamento;
    }

    @Column(name = "classificacao", insertable = false, updatable = false)
    public Double getClassificacao() {
        return classificacao;
    }

    public void setClassificacao(Double classificacao) {
        this.classificacao = classificacao;
    }

    @Override
    public String toString() {
        return "Filme [atores=" + atores + ", dataLancamento=" + dataLancamento + ", diretor=" + diretor + ", generos="
                + generos + ", id=" + id + ", nome=" + nome + ", classificacao=" + classificacao + "]";
    }
    
}
