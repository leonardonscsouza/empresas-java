package com.desafio.api.apidesafio.servicos;

import java.util.List;
import java.util.Optional;

import com.desafio.api.apidesafio.entidades.Usuario;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface UsuarioServico {
    
    /**
     * Insere um novo usuário na base de dados.
     * 
     * @param usuario
     * @return Usuario
     */
    Usuario inserirUsuario(Usuario usuario);

    /**
     * Retorna um usuário por email
     * 
     * @param email
     * @return Usuario
     */
    Usuario consultarUsuarioPorEmail(String email);

    /**
     * Retorna uma lista de usuários ativos
     * 
     * @return List<Usuario>
     */
    List<Usuario> consultarUsuariosAtivos();

    /**
     * Retorna uma lista de usuários ativos
     * 
     * @return Page<Usuario>
     */
    Page<Usuario> consultarUsuariosAtivos(PageRequest pageRequest);

    /**
     * Desativa um usuário na base de dados.
     * 
     * @param id
     */
    void desativarUsuario(Long id);

     /**
     * Ativa um usuário na base de dados.
     * 
     * @param id
     */
    void ativarUsuario(Long id);

    /**
     * Altera um usuário na base de dados.
     * 
     * @param usuario
     * @return Usuario
     */
    Usuario alterarUsuario(Usuario usuario);

    /**
     * Altera um usuário na base de dados.
     * 
     * @param usuario
     * @return <Optional>Usuario
     */
    Optional<Usuario> obterUsuarioPorId(Long id);
}
