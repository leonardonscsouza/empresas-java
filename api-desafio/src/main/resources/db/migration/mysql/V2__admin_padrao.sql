-- Usuario admin@mail.com
-- Senha 123456

INSERT INTO `usuario` (`id`, `nome`, `email`, `senha`, `cargo`, `data_nascimento`, 
`data_criacao`, `data_atualizacao`, `data_desativacao`) 
VALUES (NULL, 'Admin', 'admin@mail.com', '$2a$10$2PfjDret/dTLjhnknjX8/u03l3rR/6j.4k6SfyL4z.1PrA.P8NS96', 'ROLE_ADMIN', CURRENT_DATE(), CURRENT_DATE(), CURRENT_DATE(), NULL);
