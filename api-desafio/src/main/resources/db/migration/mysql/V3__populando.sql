INSERT INTO `usuario` (`id`, `nome`, `email`, `senha`, `cargo`, `data_nascimento`, 
`data_criacao`, `data_atualizacao`, `data_desativacao`) 
VALUES (NULL, 'Leonardo', 'leonardo@mail.com', '$2a$10$2h/EPqcgGUe6lYjsqGGrI.yLabDEGfrolyi7MBYv4buO8GIHQ0WLu', 'ROLE_USUARIO', CURRENT_DATE(), CURRENT_DATE(), CURRENT_DATE(), NULL);

INSERT INTO `usuario` (`id`, `nome`, `email`, `senha`, `cargo`, `data_nascimento`, 
`data_criacao`, `data_atualizacao`, `data_desativacao`) 
VALUES (NULL, 'Nascimento', 'nascimento@mail.com', '$2a$10$ut4kHCe3J7AM.CL89AQwHuyY3XZW/DuEZwzSdAMXXjVJT6GQUuANK', 'ROLE_USUARIO', CURRENT_DATE(), CURRENT_DATE(), CURRENT_DATE(), CURRENT_DATE());

INSERT INTO `usuario` (`id`, `nome`, `email`, `senha`, `cargo`, `data_nascimento`, 
`data_criacao`, `data_atualizacao`, `data_desativacao`) 
VALUES (NULL, 'Souza', 'souza@mail.com', '$2a$10$wcqHJYhSObwBNhtQTr2N.uPeItwpG1KPPeRmd3gOn2EPicwiLKDN2', 'ROLE_USUARIO', CURRENT_DATE(), CURRENT_DATE(), CURRENT_DATE(), NULL);

INSERT INTO `filme` (`nome`, `diretor`, `generos`, `atores`, `data_lancamento`) 
VALUES ('Os Vingadores', 'Joss Whedon', 'Ação;Ficção Científica;', 'Robert Downey Jr;Chris Evans;Scarlett Johansson;', CURRENT_DATE());

INSERT INTO `filme` (`nome`, `diretor`, `generos`, `atores`, `data_lancamento`) 
VALUES ('O Poderoso Chefão', 'Francis Ford Coppola', 'Crime;Drama;', 'Marlon Brando;Al Pacino;Diane Keaton', CURRENT_DATE());

INSERT INTO `filme` (`nome`, `diretor`, `generos`, `atores`, `data_lancamento`) 
VALUES ('O Iluminado', 'Stanley Kubrick', 'Terror;Suspense;Drama;', 'Jack Nicholson;Shelley Duvall;Danny Lloyd', CURRENT_DATE());

INSERT INTO `filme` (`nome`, `diretor`, `generos`, `atores`, `data_lancamento`) 
VALUES ('Star Wars: Episódio IV – Uma Nova Esperança', 'George Lucas', 'Aventura;Épico;Ficção Científica;', 'Mark Hamill;Harrison Ford;Carrie Fisher', CURRENT_DATE());

